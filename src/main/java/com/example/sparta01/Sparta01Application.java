package com.example.sparta01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sparta01Application {

    public static void main(String[] args) {
        SpringApplication.run(Sparta01Application.class, args);
    }

}
